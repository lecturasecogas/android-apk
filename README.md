# android-apk

## PRODUCTION
### Release 1.0.0
Primer salida a produccion.
### Release 1.0.1

Fix: Reenviar segmento cuando hay error de conexion a internet

### Release 1.0.1

Fix: No mostrar cartel de upgrade version de apk


### Release 1.0.2

Fix:
* Marcar segmento como error cuando no se pudo devolver al portal.

### Release 1.0.3

Fix:
* Correcion en timeout de envios de fotos, se aumenta el tiempo en 30 de espera por cada foto.
* En caso de error(conexion, foreground/background), forzar REINTENTAR
* En caso de perdida de conexion marcar segmento como ERROR

### Release 1.0.4

Fix: Coneccion a todos los servicios por http.

## QAS

### Release 1.0.0
Primer salida a produccion.
### Release 1.0.1

Fix: Reenviar segmento cuando hay error de conexion a internet

### Release 1.0.2

Fix: No mostrar cartel de upgrade version de apk


### Release 1.0.3

Fix:
* Marcar segmento como error cuando no se pudo devolver al portal.

### Release 1.0.4

Fix:
* Correcion en timeout de envios de fotos, se aumenta el tiempo en 10 de espera por cada foto.
* En caso de error(conexion, foreground/background), forzar REINTENTAR
* En caso de perdida de conexion marcar segmento como ERROR

### Release 1.0.5

Fix:
* Correcion en timeout de envios de fotos, se aumenta el tiempo en 30 de espera por cada foto.

### Release 1.0.11

Fix:
* Vamos por http en QAS.README.md